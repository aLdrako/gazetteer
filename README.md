# Gazetteer

Gazetteer - This application is envisaged as a “mobile first“ website that will operate equally well on desktop computers. It will provide profiling for all countries through the presentation of demographic, climatic, geographical and other data.

## Project Overview

This document describes the core functionality of the proposed system along with a
suggestion of how the website may be presented. The final design and mode of operation
is down to you and you will be expected to exploit many of the techniques that you have
learnt in the Code Academy courses.

## Minimum skill set

The ability to deploy the following will be required to complete this project:
 - HTML / CSS (Bootstrap framework)
 - JavaScript
 - JQuery DOM manipulation / AJAX
 - PHP cURL

## Additional resources

The following third party APIs are recommended (and all of them have restricted limits for free plans) but you may decide to use others that you are already familiar with or have identified as suitable replacements:
 - OpenCage
 - Open Weather
 - Geonames
 - Rest Countries
 - Open Exchange Rates

Additionally, deployment of the open source Leaflet library will be required to display maps and overlays of your data. Introductory tutorials may be found here.
